package com.example.demo.dto;

import lombok.Data;

@Data
public class DogDto {
    private Long id;
    private String breed;
    private String subBreed;
}
