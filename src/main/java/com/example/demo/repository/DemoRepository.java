package com.example.demo.repository;

import com.example.dogservice.entity.Dog;
import org.springframework.data.jpa.repository.JpaRepository;

public class DemoRepository extends JpaRepository<Dog, Long> {

}
