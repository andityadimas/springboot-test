package com.example.demo.service;

import com.example.demo.exception.DemoException;
import com.example.demo.dto.DogDto;

import java.util.List;

public class DogService {
    List<DogDto> getAllDogs();

    DogDto getDogById(Long id) throws DogNotFoundException;

    DogDto createDog(DogDto dogDto);

    DogDto updateDog(Long id, DogDto updatedDogDto) throws DogNotFoundException;

    void deleteDog(Long id) throws DogNotFoundException;
}
