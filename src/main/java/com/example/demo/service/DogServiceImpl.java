package com.example.demo.service;

import com.example.demo.dto.DogDto;
import com.example.demo.entity.Dog;
import com.example.demo.exception.DemoException;
import com.example.demo.repository.DemoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

public class DemoServiceImpl implements DemoService {
    private final DemoRepository dogRepository;

    @Autowired
    public DemoServiceImpl(DemoServiceImpl dogRepository) {
        this.dogRepository = dogRepository;
    }

    @Override
    public List<DogDto> getAllDogs() {
        return dogRepository.findAll().stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public DogDto getDogById(Long id) throws DogNotFoundException {
        Dog dog = dogRepository.findById(id)
                .orElseThrow(() -> new DogNotFoundException("Dog not found with ID: " + id));
        return mapToDto(dog);
    }

    @Override
    public DogDto createDog(DogDto dogDto) {
        Dog dog = mapToEntity(dogDto);
        Dog savedDog = dogRepository.save(dog);
        return mapToDto(savedDog);
    }

    @Override
    public DogDto updateDog(Long id, DogDto updatedDogDto) throws DogNotFoundException {
        Dog existingDog = dogRepository.findById(id)
                .orElseThrow(() -> new DogNotFoundException("Dog not found with ID: " + id));

        existingDog.setBreed(updatedDogDto.getBreed());
        existingDog.setSubBreed(updatedDogDto.getSubBreed());

        Dog updatedDog = dogRepository.save(existingDog);
        return mapToDto(updatedDog);
    }

    @Override
    public void deleteDog(Long id) throws DogNotFoundException {
        if (!dogRepository.existsById(id)) {
            throw new DogNotFoundException("Dog not found with ID: " + id);
        }
        dogRepository.deleteById(id);
    }

    private DogDto mapToDto(Dog dog) {
        DogDto dto = new DogDto();
        dto.setId(dog.getId());
        dto.setBreed(dog.getBreed());
        dto.setSubBreed(dog.getSubBreed());
        return dto;
    }

    private Dog mapToEntity(DogDto dto) {
        Dog dog = new Dog();
        dog.setId(dto.getId());
        dog.setBreed(dto.getBreed());
        dog.setSubBreed(dto.getSubBreed());
        return dog;
    }
}

@Service
public class DogServiceImpl implements DogService {

}